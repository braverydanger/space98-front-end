module.exports = function(grunt) {

  // Now we don't have to manually declare each dependency.  Yay.
  require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

  // Project config
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    watch: {
      options: {
        livereload: true,
      },
      css: {
        files: [
        '**/*.sass',
        '**/*.scss'
        ],
        tasks: ['clean', 'compass', 'autoprefixer', 'cssmin' ],
      },

      scripts: {
        files: [
        'js/src/common.js',
        'Gruntfile.js',
        "app/bower_components/angular-resource/angular-resource.js",
        "app/bower_components/angular-sanitize/angular-sanitize.js",
        "app/bower_components/angular-route/angular-route.js",
        "app/bower_components/angular-flexslider/angular-flexslider.js",
        "app/bower_components/flexslider/jquery.flexslider-min.js",
        "app/bower_components/angular-cookies/angular-cookies.js",
        "app/scripts/helpers/helperFunctions.js",
        "app/scripts/app.js",
        "app/scripts/directives/directives.js",
        "app/scripts/controllers/main.js",
        "app/scripts/factories/blogActions.js",
        "app/scripts/factories/viewActions.js",
        "app/scripts/factories/rootFunctions.js",
        "app/scripts/factories/blogGatherers.js",
        "app/scripts/services/blogService.js",
        "app/scripts/filters/customFilters.js",
        "app/scripts/ui_scripts.js"
        ],
        tasks: ['uglify', 'concat'],
      },

    },

    compass: {
      dist: {
        options: {
          sassDir: 'css/src/',
          cssDir: 'css/dest/',
          outputStyle: 'compressed'
        }
      }
    },

    autoprefixer: {
      no_dest_single: {
        src: 'css/dest/base.css'
      },
    },

    cssmin: {
      minify: {
        expand: true,
        cwd: 'css/dest/',
        src: ['*.css'],
        dest: 'css/dest/',
        ext: '.min.css'
      }
    },

    // begin JS settings
    uglify: {
        options: {
            mangle: false
        },
        my_target: {
            files: {
                'js/dest/common.min.js': ['js/src/common.js'],
                'js/dest/vendorScripts.js' : [
                    "app/bower_components/angular-resource/angular-resource.js",
                    "app/bower_components/angular-sanitize/angular-sanitize.js",
                    "app/bower_components/angular-route/angular-route.js",
                    "app/bower_components/angular-flexslider/angular-flexslider.js",
                    "app/bower_components/flexslider/jquery.flexslider-min.js",
                    "app/bower_components/angular-cookies/angular-cookies.js",
                ],
                'js/dest/appScripts.js' : [
                	"app/scripts/ui_scripts.js",
                    "app/scripts/helpers/helperFunctions.js",
                    "app/scripts/app.js",
                    "app/scripts/directives/directives.js",
                    "app/scripts/controllers/main.js",
                    "app/scripts/factories/blogActions.js",
                    "app/scripts/factories/viewActions.js",
                    "app/scripts/factories/rootFunctions.js",
                    "app/scripts/factories/blogGatherers.js",
                    "app/scripts/services/blogService.js",
                    "app/scripts/filters/customFilters.js"
                ]
            }
        }
    },

    concat: {
        my_target: {
            files: {
                'js/dest/common.min.js': ['js/src/common.js'],
                'js/concat/vendorScripts.js' : [
                    "app/bower_components/angular-resource/angular-resource.js",
                    "app/bower_components/angular-sanitize/angular-sanitize.js",
                    "app/bower_components/angular-route/angular-route.js",
                    "app/bower_components/angular-flexslider/angular-flexslider.js",
                    "app/bower_components/flexslider/jquery.flexslider-min.js",
                    "app/bower_components/angular-cookies/angular-cookies.js",
                ],
                'js/concat/appScripts.js' : [
                	"app/scripts/ui_scripts.js",
                    "app/scripts/helpers/helperFunctions.js",
                    "app/scripts/app.js",
                    "app/scripts/directives/directives.js",
                    "app/scripts/controllers/main.js",
                    "app/scripts/factories/blogActions.js",
                    "app/scripts/factories/viewActions.js",
                    "app/scripts/factories/rootFunctions.js",
                    "app/scripts/factories/blogGatherers.js",
                    "app/scripts/services/blogService.js",
                    "app/scripts/filters/customFilters.js"
                ]
            }
        }
    },

    // delete the unminified css file
    clean: ["css/dest/"],

  });

  // for the default 'grunt' task, run all other tasks before watch so the site is completely compiled immediately
  grunt.registerTask('default', [ 'clean', 'compass', 'autoprefixer', 'cssmin', 'uglify', 'watch']);

};
