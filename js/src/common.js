(function($) {
	$(document).ready(function(){
		if(getCookie('alreadyVisited')){
			$('body').addClass('already-visited');
		}

		$('body').on("resize", function(e){
			console.log(e);
		})
	});

    function setCookie(c_name,value,exdays)
    {
      var exdate=new Date();
      exdate.setDate(exdate.getDate() + exdays);
      var c_value=escape(value) +
        ((exdays==null) ? "" : ("; expires="+exdate.toUTCString()));
      document.cookie=c_name + "=" + c_value;
    }

    function getCookie(c_name)
    {
     var i,x,y,ARRcookies=document.cookie.split(";");
     for (i=0;i<ARRcookies.length;i++)
     {
      x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
      y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
      x=x.replace(/^\s+|\s+$/g,"");
      if (x==c_name)
      {
       return unescape(y);
      }
     }
    }

	$(document).on("click", ".wrapperContent", function() {
		hideSplash();
	});



	$(window).bind('scroll', function(){
		hideSplash();
	 	$(window).unbind('scroll')
	});

	$('footer').on("click", ".time a", function(e) {
		e.preventDefault();
		$('footer .graybox').removeClass('active');
		$('footer').find( $(this).attr('href') ).addClass('active');
	});

	function hideSplash(){
		$(".hphide").show()
	 	$('.splashWrapper').slideUp('slow', function(){});
	 	setCookie('alreadyVisited','true','99');
	}

})(jQuery);
