// include gulp
var gulp = require('gulp'); 

// include plugins
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var compass = require('gulp-compass');
var notify = require('gulp-notify');
var minifycss = require('gulp-minify-css');
var livereload = require('gulp-livereload');
var lr = require('tiny-lr');

var server = lr();

// set up sources for angular app and vendor scripts
var appSource = [
	"app/scripts/ui_scripts.js",
    "app/scripts/helpers/helperFunctions.js",
    "app/scripts/app.js",
    "app/scripts/directives/directives.js",
    "app/scripts/controllers/main.js",
    "app/scripts/factories/blogActions.js",
    "app/scripts/factories/viewActions.js",
    "app/scripts/factories/rootFunctions.js",
    "app/scripts/factories/blogGatherers.js",
    "app/scripts/services/blogService.js",
    "app/scripts/filters/customFilters.js"
];
var vendorSource = [
	"app/bower_components/angular-resource/angular-resource.js",
    "app/bower_components/angular-sanitize/angular-sanitize.js",
    "app/bower_components/angular-route/angular-route.js",
    "app/bower_components/angular-flexslider/angular-flexslider.js",
    "app/bower_components/flexslider/jquery.flexslider-min.js",
    "app/bower_components/angular-cookies/angular-cookies.js"
];

// compile sass
gulp.task('sass', function() {
    return gulp.src('css/src/base.scss')
        //.pipe(sass()) // use npm's default sass compiler; super fast!
        .pipe(compass({ // use compass because it's rad, albeit slightly slower :(
        	// it also makes you specify source and destination explicitly
            css: 'css/dest', // destination directory for css files
            sass: 'css/src' // source directory for sass files
        }))
		.pipe(minifycss())
		.pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('css/dest'))
        .pipe(livereload(server))
        .pipe(notify({
        	message: 'Sass compiled!'
        }));
});

// trigger livereload when any view/template files are updated
gulp.task('views', function(){
	return gulp.src('') // not modifying files, but need something to pipe to livereload
		.pipe(livereload(server));
	console.log('view file updated');
});

// minify non-angular scripts
gulp.task('scripts', function() {
    return gulp.src(['js/src/*.js'])
        .pipe(rename({suffix: '.min'}))
        .pipe(uglify({outSourceMap: true}))
        .pipe(gulp.dest('js/dest'))
        .pipe(livereload(server))
        .pipe(notify({
        	message: 'JS minified!'
        }));
});

// concat and minify angular app scripts
gulp.task('app', function() {
    return gulp.src(appSource)
        //.pipe(rename({suffix: '.min'}))
        .pipe(concat('appScripts.js'))
        .pipe(uglify({outSourceMap: true}))
        .pipe(gulp.dest('js/dest'))
        .pipe(livereload(server))
        .pipe(notify({
        	message: 'app scripts minified!'
        }));
});

// concat and minify angular vendor scripts
gulp.task('vendor', function() {
    return gulp.src(vendorSource)
        //.pipe(rename({suffix: '.min'}))
        .pipe(concat('vendorScripts.js'))
        .pipe(uglify({outSourceMap: true}))
        .pipe(gulp.dest('js/dest'))
        .pipe(livereload(server))
        .pipe(notify({
        	message: 'vendor scripts minified!'
        }));
});

// watch for changes
gulp.task('watch', function() {
	
	server.listen(35729, function (err) {
        if (err) {
            return console.log(err)
        };

        gulp.watch('js/src/*.js', ['scripts']);
        gulp.watch(appSource, ['app']);
        gulp.watch(vendorSource, ['vendor']);
	    gulp.watch('css/src/*.scss', ['sass']);
	    gulp.watch(['templates/angular/*.html', 'templates/angular/**/*.html'], ['views']);
	    
    });
});

// Default Task
gulp.task('default', ['sass', 'scripts', 'app', 'vendor', 'watch']);