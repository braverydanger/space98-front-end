space98.service('blogService', ['$resource', 'app_settings', '$q', '$routeParams', function($resource, app_settings, $q, $routeParams) {

		var _self = {};

		/*
if($routeParams.getProdDB === "1") {
			//app_settings.services.blog.base = "http://www.spaceninety8.com/api/v0/blog"
			app_settings.services.blog.base = "http://edts.weighted-urbanoutfitters.com.akadns.net/s98/v0/blog"
		}
*/

		var switchServiceBase = app_settings.isDevSite &&
			(($routeParams.getProdDB && $routeParams.getProdDB === "1")
			|| (app_settings.storageOptions
				&& localStorage.serviceBase === "dev"))

		var serviceUrls = {
			base : switchServiceBase ?  app_settings.services.blog.DEV : app_settings.services.blog.base
		}

		var resourceList = (function(){
			var endpoints = {
				master : "/posts",
				blog : "/type/post/posts",
				taxonomy : "/type/post/posts",
				marketSpace : "/type/marketspace_type/posts",
				gallery : "/type/gallery_type/posts",
				events : "/type/event_type/posts",
				page : "",
				postById : "/posts/:postId",
				postBySlug : "/posts/slug/:postId"
			}

			for (var e in endpoints) {
				endpoints[e] = serviceUrls.base + endpoints[e];
			}

			return endpoints;
		})()

		_self.postGetter = function(resource, success, error, options, getById) {
			resource = (resource === "singlePost")
				? !(parseInt(options.postId).toString().length === options.postId.toString().length)
					? "postBySlug"
					: "postById"
				: resource
			return $resource(resourceList[resource]).get(options, success, error);
		}

		_self.postGetterById = function(resource, success, error, options) {
			res = !(parseInt(options.postId).toString().length === options.postId.toString().length)
				? "postBySlug"
				: "postById";
			return $resource(resourceList[res]).get(options, success, error);
		}

		return _self;

	}])
