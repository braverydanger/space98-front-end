angular.module('customFilters', []).filter('hyphenate', function() {
	return function(input) {
		var replacedString;
		if(typeof input !== 'undefined'){
			if(input.length > 0){
				replacedString = input.replace(' ','-');
			}
		}
		return replacedString
	};
});