space98.factory('rootFunctions', 
['app_settings', 'messaging', '$timeout', '$rootScope', '$sce', '$routeParams',
function(pdp_settings, messaging, $timeout, $rootScope, $sce, $routeParams) {
	var _self = {};
	
	_self.trustHtml = function(block) {
		return $sce.trustAsHtml(block);
	}
	
	_self.pagination = function(pagObject) {
		_this = {};
		
		var p = {
			currentPage : pagObject.currentPage ? pagObject.currentPage : 1,
			offset : pagObject.offset ? pagObject.offset : 10,
			pageType: pagObject.pageType ? pagObject.pageType : "posts",
			totalCount : pagObject.totalCount ? pagObject.totalCount : 0
		}
		
		var makeLink = function(page) {
			var opts = {};
			page > 1 && (opts.page = page);
			$routeParams.offset && (opts.offset = $routeParams.offset);
			var ub = urlBuilder(p.pageType, opts);
			return ub;
		}
		
		_this.p = p;
		_this.po = pagObject;
		
		_this.prev = p.currentPage > 1 ? makeLink(p.currentPage - 1) : false;
		_this.next = p.totalCount > (p.offset*p.currentPage) ? makeLink(p.currentPage + 1) : false;
		return _this;
	}
	
	_self.convertLineBreaks = function(str) {
		if (typeof str === 'undefined' || !str || str == "") { return false }
		str = str.replace(/\r\n\r\n/g, "</p><p>").replace(/\n\n/g, "</p><p>");
		str = str.replace(/\r\n/g, "<br />").replace(/\n/g, "<br />");
		return str;
	}
		
	return _self;
}]);
