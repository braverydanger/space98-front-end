space98.factory('blogGatherers',
['app_settings', 'messaging', '$timeout', '$rootScope', '$sce', 'blogService', 'messaging', '$routeParams', '$q',
function(app_settings, messaging, $timeout, $rootScope, $sce, blogService, messaging, $routeParams, $q) {
	var _self = {};

	_self.gatherImagesFromAttachments = function(post, imageList, runHailMary) {
		var fi = [];
		if (!angular.isDefined(post.attachmentObj)) { return false }
		var queueList = ['thumbnail', 'postThumbnail', 'medium', 'large', 'twentyfourteenFullWidth', 'full'];
		var imageQueue = function(start, attImages) {
			var retImg = attImages.url;
			for (var i = start; i < queueList.length; i++) {
				if (angular.isDefined(attImages.images)
					&& angular.isDefined(attImages.images[queueList[i]])
					&& angular.isDefined(attImages.images[queueList[i]].url)
				) {
					retImg = angular.isDefined(attImages.images[queueList[i]].url) ? attImages.images[queueList[i]].url : false;
					return retImg;
				}
			}
			return retImg;
		}

		var checkImage = function(attObj, size, level) {
			return angular.isDefined(attObj) && angular.isDefined(attObj.images) && angular.isDefined(attObj.images[size])
					? attObj.images[size].url
					: imageQueue(level, attObj);
		}

		var hailMaryImage = function() {
			if (post.attachments.length > 0 && runHailMary) {
				var obj = {};
				obj.small =  checkImage(post.attachments[0], 'thumbnail', 0);
				obj.medium =  checkImage(post.attachments[0], 'medium', 2);
				obj.large =  checkImage(post.attachments[0], 'large', 3);
				obj.full =  checkImage(post.attachments[0], 'full', 4);
				return [obj];
			}
			return false;
		}

		if (imageList) {
			for (var i = 0; i < imageList.length; i++) {
				var fiKey = imageList[i];
				if (!(fiKey === '' || typeof fiKey === "undefined")) {
					fi[i] = {};
					fi[i].small =  checkImage(post.attachmentObj[fiKey], 'thumbnail', 0);
					fi[i].medium =  checkImage(post.attachmentObj[fiKey], 'medium', 2);
					fi[i].large =  checkImage(post.attachmentObj[fiKey], 'large', 3);
					fi[i].full =  checkImage(post.attachmentObj[fiKey], 'full', 5);
				}
			}
			return fi.length > 0 ? fi : hailMaryImage();
		}
		return hailMaryImage() ? hailMaryImage() : false;
	}

	_self.getSlideShowImages = function(post) {
		var fi = [];
		var gal = post.customFields.imageGallery;
		if (!angular.isDefined(post.attachmentObj) || !angular.isDefined(gal)) { return false }
		for (var gi in gal) {
			gal[gi].image = post.attachmentObj[gal[gi].image].url
		}
		return false;
	}

	_self.prettyDateObj = function(dateString) {
		var dateObj = {
			fullString : dateString
		}
		var getDateArray = function(ds) {
			var d = ds.split(" ")[0],
				t = ds.split(" ")[1]
				dA = d.split("-");
				tA = t.split(":");
			ds = [dA[0], dA[1], dA[2], tA[0], tA[1], tA[2]];
			for (var i = 0; i < ds.length; i++) {
				ds[i] = parseInt(ds[i]);
			}
			return ds;
		}
		var ds = getDateArray(dateString);
		dateObj.timestamp = Date.UTC(ds[0],(ds[1]-1),ds[2],ds[3],ds[4]);
		return dateObj;
	}

	_self.getDateRange = function(obj) {
		// January 1, 2060 === 2051221000
		var ocf = obj.customFields
		var sd = ocf.startDate
			&& angular.isArray(ocf.startDate)
			&& angular.isNumber(ocf.startDate[0].timestamp)
			&& ocf.startDate[0].timestamp < 2051221000
				? ocf.startDate[0].timestamp*1000
				: false;

		var ed = ocf.endDate
			&& angular.isArray(ocf.endDate)
			&& angular.isNumber(ocf.endDate[0].timestamp)
			&& ocf.endDate[0].timestamp < 2051221000
				? ocf.endDate[0].timestamp*1000
				: false;

		return sd ? {start: sd, end: ed, permanent: (sd && !ed)} : false;
	}

	_self.requiredFieldTest = function(obj) {
		var requiredFields = [
			"title",
			"customFields"
		];
		for (var i = 0; i < requiredFields.length; i++) {
			if (!obj.hasOwnProperty(requiredFields[i])
				|| !obj[requiredFields[i]]
				|| !angular.isDefined(obj[requiredFields[i]])
				|| typeof obj[requiredFields[i]] === "undefined"
				|| obj[requiredFields[i]] === null
				|| (angular.isArray(obj[requiredFields[i]]) && obj[requiredFields[i]].length < 1)
			) {
				return false;
			}
		}

		return true;
	}

	_self.matchesTopPost = function(slug) {
		return $routeParams.postId === slug;
	}


	_self.sortBy = function(args) {
		if (angular.isDefined(args.parentFeed) && angular.isDefined(args.innerArgs)) {
			args.parentFeed.sort(function(a,b) {
				return a.customFields.startDate[0].timestamp - b.customFields.startDate[0].timestamp
			})
			return args.parentFeed;
		}
		return false;
	}

	_self.relevantDate = function(args) {

		return args.parentFeed;
	}

	_self.alphabetList = function(args) {
		if (angular.isDefined(args.parentFeed) && angular.isDefined(args.innerArgs)) {
			return true;
		}
		return false;
	}

	return _self;
}]);
