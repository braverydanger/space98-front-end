space98.factory('blogActions',
['app_settings', 'messaging', '$timeout', '$rootScope', '$sce', 'blogService', 'messaging', '$routeParams', 'blogGatherers',
function(app_settings, messaging, $timeout, $rootScope, $sce, blogService, messaging, $routeParams, blogGatherers) {
	var _self = {};
	var now = Math.floor(new Date().getTime()/1000);
	_self.getPosts = function(elem, success, error) {
		var o = {};
		var endpoint = "";
		switch (elem.method) {
			case "getPost" :
				endpoint = "singlePost"
				o.postId = elem.options && angular.isDefined(elem.options.postId)
					? elem.options.postId
					: angular.isDefined($routeParams.postId)
						? $routeParams.postId
						: false;
			break;
			default:
				endpoint = elem.endpoint
				if (elem.options) {
					for (var opt in elem.options) {
						o[opt] = elem.options[opt];
					}
					o.offset = $routeParams.offset ? $routeParams.offset : elem.options.offset;
					if ($routeParams.page || elem.options.page) {
						o.page = (parseInt(($routeParams.page ? $routeParams.page : elem.options.page))-1)*o.offset;
						o.page = o.page > 0 ? o.page : 0;
					}
				} else {
					o.offset = $routeParams.offset ? $routeParams.offset : 10;
					o.page = ($routeParams.page ? parseInt($routeParams.page)-1 : 0)*o.offset;
				}
			break;
		}

		var retFeed = blogService.postGetter(endpoint, function(res){
			var feedOptions = angular.isDefined(elem.feedOptions) ? elem.feedOptions : false;
			if (angular.isDefined(res.post)) {
				_self.digest.single(res.post, feedOptions);
				res.topPost = res.post;
			} else {
				_self.digest.list(res, feedOptions);
			}
			success(res, elem)
		}, function(res) {
			error(res, elem)
		}, o)
		return retFeed;
	}


	_self.digest = {
		single : function(obj, isFromList) {
			obj.canShow =
				blogGatherers.requiredFieldTest(obj)
					&& ((isFromList && !blogGatherers.matchesTopPost(obj.slug))
						|| !isFromList);

			obj.attachmentObj = keyObjectInArray(obj.attachments, "id");

			obj.featuredImages =
				obj.customFields.featuredImages && obj.customFields.featuredImages.length > 0
					? blogGatherers.gatherImagesFromAttachments(obj, obj.customFields.featuredImages, true)
					: false;

			obj.postThumbnails =
				obj.customFields.postThumbnails
				&& obj.customFields.postThumbnails.length > 0
				&& !!(blogGatherers.gatherImagesFromAttachments(obj, obj.customFields.postThumbnails))
					? blogGatherers.gatherImagesFromAttachments(obj, obj.customFields.postThumbnails)
					: obj.featuredImages;

			obj.date = blogGatherers.prettyDateObj(obj.date);
			obj.dateRange = blogGatherers.getDateRange(obj);
			obj.slideShow =
				angular.isDefined(obj.customFields.displayTemplate)
				&& obj.customFields.displayTemplate === '0'
					? getObjectSize(blogGatherers.getSlideShowImages(obj)) > 0
						? blogGatherers.getSlideShowImages(obj)
						: false
					: false

			obj.singlePost = !isFromList;
			if (obj.singlePost) {

			}
			return obj;
		},
		list : function(response, flags) {
			resArr = [];
			for (var i = 0; i < response.posts.length; i++) {
				response.posts[i] = _self.digest.single(response.posts[i], true);
				if (response.posts[i].canShow) { resArr.push(response.posts[i]) }
			}


			if (flags) {
				if (angular.isObject(flags)) {
					for (var f in flags) {
						var args = { parentFeed: resArr, innerArgs : flags[f] };
						blogGatherers[f].apply(this, [args]);
					}
				}
			}
			response.posts = resArr;
			return response;
		}
	}


	return _self;
}]);
