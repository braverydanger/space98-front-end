space98.controller(
	'MainCtrl',
	['$rootScope', '$scope', '$routeParams', '$location', '$route', 'blogActions', 'viewActions', 'rootFunctions', '$cookies', 'app_settings',
	function
	( $rootScope, $scope, $routeParams, $location, $route, blogActions, viewActions, rootFunctions, $cookies, app_settings ) {

		$scope.pageParams = $route.current.$$route.pageParams;
		$scope.pageData = $scope.pageParams.endpoints;

		// set location for social share
		$rootScope.escapedLocation = encodeURIComponent($location.$$absUrl);
		$rootScope.location = $location.$$absUrl;
		$scope.viewActions = viewActions;

		var pagination =  {
			currentPage : $routeParams.page ? parseInt($routeParams.page) : 1,
			offset : 10,
			pageType: "posts",
			totalCount : 0
		}

		for( var elem in $scope.pageData ) {
			blogActions.getPosts( $scope.pageData[elem],
				function(res, retElem){
					if (angular.isDefined(res.topPost)) {
						$scope.topPost = res.topPost;
						if ($scope.pageParams.type === "post") {
							$rootScope.title = $rootScope.title + res.topPost.title;
							$rootScope.pageSlug = res.topPost.type;
						}
					}
					retElem.data = res;
					$scope.pageParams.type === "listing" && setPagination(retElem);
				},
				function(res, retElem){
					$scope.topPost = false;
					retElem = false;
				})
		}

		function setPagination(elem) {
			pagination.offset = angular.isDefined($routeParams.offset)
				? $routeParams.offset
				: angular.isDefined(elem.options)
					&& angular.isDefined(elem.options.offset)
					? elem.options.offset
					: pagination.offset;

			pagination.pageType = elem.pageInfo.path;
			pagination.totalCount = elem.data.meta.count;
			elem.pagObject = rootFunctions.pagination(pagination);
		}
	}
]);
