space98.constant('pagesSetup', (function(){
	var now = Math.floor(new Date().getTime()/1000);
	var pagesSetup = {
		home : {
			splash : {
				dynData : { method : "getPage", args: ["splash"] },
				pageInfo : { title : "Space 98",  path : "home" }
			},
			gorbals : {
				dynData : { method : "getPage", args: ["gorbals"] },
					pageInfo : { title : "Gorbals",  path : "gorbals" }
			},
			market : {
				dynData : { method : "getPosts", args: ["marketSpace", {offset: 20, date: now}] },
				pageInfo : { title : "Market Space", path : "market-space" },
				feedOptions : { sortBy: "customFields.startDate" }
			},
			gallery : {
				dynData : { method : "getPosts", args: ["gallery", {offset: 1, filterBy: "featuredPost"}] },
				pageInfo : { title : "Gallery 98", path : "gallery98" },
				feedOptions : { sortBy: "customFields.endDate" }
			},
			blog : {
				dynData : { method : "getPosts", args: ["blog", {offset: 9}] },
				pageInfo : { title : "Blog", path : "blog" },
				displayOptions : { listingClass : "left-align bold" }
			}
		},

		blog : {
			listing : {
				posts : {
					dynData : { method : "getPosts", args: ["blog", {offset: 15}] },
					pageInfo : { title : "Blog", path : "blog" }
				}
			},
			single : {
				post : {
					dynData : { method : "getSinglePost", args: [] },
					pageInfo : { title : "Blog", path : "posts" }
				},
				related : {
					dynData : { method : "getPosts", args: ["blog", {offset: 9}] },
					pageInfo : { title : "Blog", path : "blog" }
				}
			}
		},
		
		master : {
			listing : {
				posts : {
					dynData : { method : "getPosts", args: ["master", {offset: 20}] },
					pageInfo : { title : "All Posts", path : "posts" }
				}
			},
			single : {
				post : {
					dynData : { method : "getSinglePost", args: [] },
					pageInfo : { title : "Post", path : "posts" }
				},
				related : {
					dynData : { method : "getPosts", args: ["master", {offset: 9}] },
					pageInfo : { title : "Post", path : "posts" }
				}
			}
		},
		
		market : {
			index : {
				featured : {
					dynData : { method : "getPosts", args: ["marketSpace", {offset: 1, filterBy: "featuredPost"}] },
					pageInfo : { title : "Market Space", path : "market-space" },
					feedOptions : { sortBy: "customFields.startDate" }
				},
				current : {
					dynData : { method : "getPosts", args: ["marketSpace", {offset: 9, date : now}] },
					pageInfo : { title : "Market Space", path : "market-space" },
					feedOptions : { sortBy: "customFields.startDate" }
				},
				upcoming : {
					dynData : { method : "getPosts", args: ["marketSpace", {offset: 9, endDate : (now + 31536000), startDate: (now + 3600)}] },
					pageInfo : { title : "Market Space", path : "market-space" },
					feedOptions : { sortBy: "customFields.startDate" }
				}
			},
			listing : {
				posts : {
					dynData : { method : "getPosts", args: ["marketSpace", {offset: 20}] },
					pageInfo : { title : "Market Space", path : "market-space/vendors" }
				}
			},
			single : {
				post : {
					dynData : { method : "getSinglePost", args: [{post_title: "marketspace_title"}] },
					pageInfo : { title : "Market Space", path : "posts" }
				},
				related : {
					dynData : { method : "getPosts", args: ["marketSpace", {offset: 9}] },
					pageInfo : { title : "Market Space", path : "market-space/vendors" },
					feedOptions : { sortBy: "customFields.startDate" }
				}
			}
		},
		
		gallery : {
			index : {
				featured : {
					dynData : { method : "getPosts", args: ["gallery", {offset: 1, filterBy: "featuredPost", date: now}] },
					pageInfo : { title : "Gallery", path : "gallery98" }
				},
				current : {
					dynData : { method : "getPosts", args: ["gallery", {offset: 9, date : now}] },
					pageInfo : { title : "Gallery", path : "gallery98" },
					feedOptions : { sortBy: "customFields.startDate" }
				},
				upcoming : {
					dynData : { method : "getPosts", args: ["gallery", {offset: 9, endDate : (now + 31536000), startDate: (now + 3600)}] },
					pageInfo : { title : "Gallery", path : "gallery98" },
					feedOptions : { sortBy: "customFields.startDate" }
				}
			},
			listing : {
				posts : {
					dynData : { method : "getPosts", args: ["gallery", {offset: 20}] },
					pageInfo : { title : "Gallery", path : "gallery98/showings" }
				}
			},
			single : {
				post : {
					dynData : { method : "getSinglePost", args: [{post_title : "gallery_title"}] },
					pageInfo : { title : "Gallery", path : "posts" }
				},
				related : {
					dynData : { method : "getPosts", args: ["gallery", {offset: 9}] },
					pageInfo : { title : "Gallery", path : "gallery98/showings" },
					feedOptions : { sortBy: "customFields.startDate" }
				}
			}
		},
		
		events : {
			index : {
				featured : {
					dynData : { method : "getPosts", args: ["events", {offset: 1, filterBy: "featuredPost", date: now}] },
					pageInfo : { title : "Events", path : "events" }
				},
				current : {
					dynData : { method : "getPosts", args: ["events", {offset: 9, date : now}] },
					pageInfo : { title : "Events", path : "events" },
					feedOptions : { sortBy: "customFields.startDate" }
				},
				upcoming : {
					dynData : { method : "getPosts", args: ["events", {offset: 9, endDate : (now + 31536000), startDate: (now + 3600)}] },
					pageInfo : { title : "Events", path : "events" },
					feedOptions : { sortBy: "customFields.startDate" }
				}
			},
			listing : {
				posts : {
					dynData : { method : "getPosts", args: ["events", {offset: 20}] },
					pageInfo : { title : "Events", path : "events" }
				}
			},
			single : {
				post : {
					dynData : { method : "getSinglePost", args: [{post_title : "events_title"}] },
					pageInfo : { title : "Events", path : "posts" }
				},
				related : {
					dynData : { method : "getPosts", args: ["events", {offset: 9}] },
					pageInfo : { title : "Events", path : "events" },
					feedOptions : { sortBy: "customFields.startDate" }
				}
			}
		},
		
		gorbals : {
			index : {
				page : {
					dynData : { method : "getPage", args: ["gorbals"] },
					pageInfo : { title : "Gorbals",  path : "gorbals" }
				}
			},
			menu : {
				page : {
					dynData : { method : "getPage", args: ["gorbals-menu"] },
					pageInfo : { title : "Gorbals Menu", path : "gorbals/menu" }
				},
				related : {
					dynData : { method : "getPosts", args: ["events", {offset: 9}] },
					pageInfo : { title : "Gorbals", path : "gorbals/menu" }
				}
			},
			photos : {
				page : {
					dynData : { method : "getPage", args: ["gorbals-photos"] },
					pageInfo : { title : "Gorbals",  path : "gorbals/photos" }
				}
			}
		},
		
		links : {
			posts : {
				dynData : { method : "getPage", args: ["links"] },
				pageInfo : { title : "Links", path : "links" },
				feedOptions : { alphabetList: true }
			}
		},
		
		tags : {
			posts : {
				dynData : { method : "getPosts", args : ["taxonomy"] },
				pageInfo : { title : "tax", path : "tags" }
			}
		}
		
	}
	return pagesSetup;
})())