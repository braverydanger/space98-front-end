var space98 = angular.module('space98', [
	'ngResource',
	'ngSanitize',
	'ngRoute',
	'angular-flexslider',
	'ngCookies',
	'customFilters'
])
.config([ '$routeProvider', '$locationProvider',
	function ($routeProvider, $locationProvider) {
	if (typeof APP_ROUTES === "undefined" || !APP_ROUTES || typeof APP_ENDPOINTS === "undefined" || !APP_ENDPOINTS)  {
		var completeSystemFailure = true;
		throw 'The angular application is not defined. Please check your blogRouteSetting.py file, or your main template.';
	}

	var addRoutes = function(routeLevel, parentObj) {
		for (route in routeLevel) {
			if (routeLevel.hasOwnProperty(route)) {
				var r = routeLevel[route]
				r.currentPath = r.route[r.route.length - 1];
				r.routePath = parentObj ? parentObj.routePath + r.currentPath : r.currentPath
				r.parentTrail = parentObj ? parentObj : false;
				$routeProvider.when(r.routePath, {
					templateUrl : 'templates/angular/' + r.template[r.template.length - 1],
					controller : r.controller,
					pageParams : {
						endpoints : r.endpoints,
						title : r.title,
						description : r.description,
						type : r.type,
						innerController : r.innerController ? r.innerController : false
					}
				})
				if (r.hasOwnProperty('children')) {
					addRoutes(r.children, r)
				}
			}
		}
	}

	addRoutes(APP_ROUTES)
	$routeProvider.otherwise({
		redirectTo: '/'
	});

	$locationProvider.html5Mode(true);
}])
.config(['$locationProvider', function($location) {
	$location.hashPrefix('!');
}])
.constant('app_settings', (function(){
	var strings = {

	};

	var appUrl = {};
		appUrl.protocol = 'http://';
		appUrl.host = 'spaceninety8.com';

	var services = {};
		services.blog = {};
		services.blog.base =
			!(typeof S98_MAIN_ENDPOINT === "undefined")
			&& angular.isDefined(S98_MAIN_ENDPOINT)
			&& S98_MAIN_ENDPOINT !== ''
				? appUrl.protocol + S98_MAIN_ENDPOINT
				: appUrl.protocol + "www.spaceninety8.com/api/v0/blog";

		services.blog.DEV = "http://s98blogdev.urbanout.com/api/v1/blog";

	var pagination = {
		offset : 25,
		page : 1
	}

	var retObj = {}
	retObj.appUrl = appUrl;
	retObj.services = services;
	retObj.pagination = pagination;
	retObj.isDevSite = !(typeof IS_DEV_SITE === "undefined") && angular.isDefined(IS_DEV_SITE) && IS_DEV_SITE;
	retObj.storageOptions = typeof(Storage)!=="undefined";

	return retObj;
})())
.constant('messaging', (function(){
	var retObj = {
		defaults : {
			mainPageTitle : document.title
		}
	};
	for (var str in retObj.defaults) {
		retObj[str] = angular.copy(retObj.defaults[str]);
	}
	return retObj;
})())
.run(['$location', '$rootScope', 'rootFunctions', 'app_settings', '$routeParams', function($location, $rootScope, rootFunctions, app_settings, $routeParams) {
	$rootScope.globals = rootFunctions;
	$rootScope.headerOpen = false;

	// Location data goes here
	$rootScope.$on('$routeChangeSuccess', function (event, current, previous) {
		// Change any global data that needs to be changed
		// when the route has changed
		// Things Like:
		// --> Page tite
		if (!angular.isDefined(current.$$route)) { return false; }
		var titleString = angular.isDefined(current.$$route.pageParams.title) ? current.$$route.pageParams.title : '';
		$rootScope.title = "Space Ninety 8 - " + titleString;
		$rootScope.headerOpen = false;
		$rootScope.pageSlug = titleString;

		$rootScope.showDevOptions = app_settings.isDevSite;
		if (app_settings.isDevSite && $routeParams.switchDb) {
			localStorage.serviceBase = $routeParams.switchDb;
		}

		// Scroll to the top of the page when routes change
		window.scrollTo(0, 0);
		//*** Set the current page in the nav
	});

	//*** load in the footer

}]);
