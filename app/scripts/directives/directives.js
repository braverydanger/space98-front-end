space98.directive('removeImageDimensions',['$timeout', function ($timeout) {
	return {
		restrict: 'A',
		link: function (scope, element, attr) {
			scope.$evalAsync(function(e){
				$timeout(function(){
					element.find("img").each(function(){
						jQuery(this).attr("height", "").attr("width", "");
					});
				}, 10)
			});
			if (scope.$last === true) {
				scope.$evalAsync(function(e){
					element.find("img").each(function(){
						jQuery(this).attr("height", "").attr("width", "");
					});
				});
			}
		}
	}
}]);
