// Set up indexOf prototype for IE8
if (!Array.prototype.indexOf) {
  Array.prototype.indexOf = function (obj, fromIndex) {
    if (fromIndex == null) {
        fromIndex = 0;
    } else if (fromIndex < 0) {
        fromIndex = Math.max(0, this.length + fromIndex);
    }
    for (var i = fromIndex, j = this.length; i < j; i++) {
        if (this[i] === obj)
            return i;
    }
    return -1;
  };
}

// Set up Object.keys for older browsers
// From https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/keys
if (!Object.keys) {
  Object.keys = (function () {
    var hasOwnProperty = Object.prototype.hasOwnProperty,
        hasDontEnumBug = !({toString: null}).propertyIsEnumerable('toString'),
        dontEnums = [
          'toString',
          'toLocaleString',
          'valueOf',
          'hasOwnProperty',
          'isPrototypeOf',
          'propertyIsEnumerable',
          'constructor'
        ],
        dontEnumsLength = dontEnums.length;

    return function (obj) {
      if (typeof obj !== 'object' && (typeof obj !== 'function' || obj === null)) {
        throw new TypeError('Object.keys called on non-object');
      }

      var result = [], prop, i;

      for (prop in obj) {
        if (hasOwnProperty.call(obj, prop)) {
          result.push(prop);
        }
      }

      if (hasDontEnumBug) {
        for (i = 0; i < dontEnumsLength; i++) {
          if (hasOwnProperty.call(obj, dontEnums[i])) {
            result.push(dontEnums[i]);
          }
        }
      }
      return result;
    };
  }());
}


//quick function for getting the first object in a list of objects
function getFirstObject(obj, wholeObject) {
	for (var k in obj) {
		return wholeObject ? obj[k] : k;
	}
}

// check for empty objects
function isEmptyObj(obj) {
	for(var prop in obj) {
		if(obj.hasOwnProperty(prop)) {
			return false;
		}
	}
	return true;
}

//Get size of object
function getObjectSize(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};

function getObjectKey(obj) {
	return Object.keys(obj)[0];
};

// remove Duplicates from array
function removeDuplicates(arr) {
	if (!(typeof arr === "array")) { return arr; }
	console.log(arr);
	var testObject = {};

	for (var i = 0; i < arr.length; i++) {
		testObject[arr[i]] = 0;
	}
	arr = [];
	for (var i in testObject) {
		arr.push(i);
	}
	return arr;
};

function getURLParameter(name,url) {
	return decodeURI((RegExp(name + '=' + '(.+?)(&|$)').exec(url)||[,null])[1]);
}

// Build a url! It's fun!!
function urlBuilder(path, query, hash) {
	// path --> string
	var query = query && getObjectSize(query) > 0 ? query : false, // query --> object [optional]
		hash = hash ? hash : false, // hash --> string [optional]
		queryArr = [];
	if (!path) { return false; }
	if (query) {
		for (var q in query) {
			queryArr.push(q + "=" + encodeURIComponent(query[q]));
		}
		queryArr = queryArr.join("&");
	}
	return path + (query ? "?" + queryArr : '') + (hash ? "#" + hash : '');
}

//Angular specific query string after hashmark
function angularHashQuery(query) {
	var query = query && getObjectSize(query) > 0 ? query : false, // query --> object [optional]
		queryArr = [];
	if (typeof query === "object" && query) {
		for (var q in query) {
			queryArr.push(q + "=" + encodeURIComponent(query[q]));
		}
		queryArr = queryArr.join("&");
	}
	return (query ? "/?" + queryArr : '')
}

// Apply new values to an object's defaults
function overwriteDfaults(newObj, defaultObj) {
	console.log(newObj);
	console.log(defaultObj);
}

function getAllElementsWithAttribute(attribute) {
  var matchingElements = [];
  var allElements = document.getElementsByTagName('*');
  for (var i = 0; i < allElements.length; i++)
  {
    if (allElements[i].getAttribute(attribute))
    {
      // Element exists with attribute. Add to array.
      matchingElements.push(allElements[i]);
    }
  }
  return matchingElements;
}

function paginationObject(count, offset, page) {
	page = parseInt(page);
	return {
		next : count > (offset*page) ? page+1 : false,
		prev : page > 0 ? page-1 : false
	}
}

function objectFromString(o, s) {
    s = s.replace(/\[(\w+)\]/g, '.$1'); // convert indexes to properties
    s = s.replace(/^\./, '');           // strip a leading dot
    var a = s.split('.');
    while (a.length) {
        var n = a.shift();
        if (n in o) {
            o = o[n];
        } else {
            return;
        }
    }
    return o;
}

function keyObjectInArray(arr, key) {
	var retObj = {};
	for (var i = 0; i < arr.length; i++) {
		retObj[arr[i][key]] = arr[i];
	}

	return retObj;
}
