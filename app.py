import tornado
import tornado.web
import os
import tornado.escape
import tornado.httpclient
import json
import urllib
import time
import math

from tornado import ioloop
from tornado import httpclient
from tornado.httpclient import AsyncHTTPClient
from tornado.escape import json_encode

import blogSettings
import blogRouteSettings

MAIN_ENDPOINT = blogSettings.blogSettings['MAIN_ENDPOINT']
appRoutes = blogRouteSettings.appRoutes
appEndpoints = blogRouteSettings.blogEndpoints
http_client = httpclient.HTTPClient()

class PageHandler(tornado.web.RequestHandler):

    def initialize(self, pageOpts):
        self.serviceData = {}
        self.endpointSize = [0,0]
        self.helpers = DataHelpers();
        self.templateData = {
            'settings' : pageOpts,
            'mainEndpoint' : MAIN_ENDPOINT,
            'dynData' : self.serviceData,
            'routeList' : appRoutes,
            'endpoints' : appEndpoints,
            'templatePath' : blogSettings.templatePath,
            'samplePath' : 'snippets/smallBucket.html'
        }

    def get(self, **params):
        self.urlParams = params
        self.endpointSize = [0,0]
        try :
            self.gatherPageData(params)
        except:
            self.handleHTTPError()
        #self.finish()
        return

    def gatherPageData(self, params):

        eps = self.templateData['settings']['endpoints']
        self.endpointSize[0] = len(eps)
        if (len(eps) > 0):
            for key, val in eps.iteritems():
                svcUrl = self.helpers.BuildServiceUrl(val, params)
                self.serviceData[key] = svcUrl
                args = {'keyHold' : key}
                try:
                    data = http_client.fetch(svcUrl)
                    self.handlePageDataResponse(data, args)
                except httpclient.HTTPError:
                    self.handleHTTPError()
        else:
            self.displayPage()

        return

    def handlePageDataResponse(self, response, args):
        self.endpointSize[1] += 1
        self.serviceData[args['keyHold']] = tornado.escape.json_decode(response.body)
        if self.endpointSize[0] == self.endpointSize[1]:
            self.displayPage()
        return

    def handleHTTPError(self):
        self.set_status(404)
        self.render(blogSettings.templatePath + '/404.html', **self.templateData)

    def displayPage(self):
        self.set_header("Content-Type", 'text/html; charset="utf-8"')
        if self.templateData['settings']['type'] == 'post' and self.urlParams['postId'] == None:
            self.templateData['settings']['template'][0] = blogRouteSettings.defaultPostsTemplate[0]
            self.templateData['dynData']['posts'] = self.templateData['dynData']['post']
        template = blogSettings.templatePath + '/' + self.templateData['settings']['template'][0]
        self.render(template, **self.templateData)


class DefaultHandler(tornado.web.RequestHandler):
    """Generates an error response with status_code for all requests."""

    def write_error(self, status_code, **kwargs):
        print 'In get_error_html. status_code: ', status_code
        if status_code > 299:
            templateData = {
                'settings' : {
                    'title' : '404 - Page not found',
                    'description' : "This is not the page you are looking for"
                },
                'mainEndpoint' : MAIN_ENDPOINT
            }
            self.render(blogSettings.templatePath + '/404.html', **templateData)


class HealthCheckHandler(tornado.web.RequestHandler):
    def get(self):
        self.set_status(200)

    def head(self):
        self.set_status(200)


class DataHelpers:
    def checkIfSlug(self, postId):
        try:
            float(postId)
        except ValueError:
            postId = "slug/" + postId
        return postId

    def BuildServiceUrl(self, endpoint, params):
        method = endpoint['method']
        urlString = "http://" + MAIN_ENDPOINT

        if method == 'getList':
            urlString += appEndpoints[endpoint['endpoint']]
        elif method == 'getPost':
            if 'options' in endpoint and 'postId' in endpoint['options']:
                self.checkIfSlug(endpoint['options']['postId'])
                urlString += appEndpoints['postById'] + "/" + self.checkIfSlug(endpoint['options']['postId'])
            elif params and params['postId']:
                self.checkIfSlug(params['postId'])
                urlString += appEndpoints['postById'] + "/" + self.checkIfSlug(params['postId'])
            else :
                urlString += '/posts'
        else:
            urlString += '/posts'

        if 'options' in endpoint:
            now = int(math.floor(time.time()))
            if 'date' in endpoint['options'] and not 'dateSet' in endpoint['options']:
                endpoint['options']['dateSet'] = 1
                endpoint['options']['date'] += now
            if 'startDate' in endpoint['options'] and not 'startDateSet' in endpoint['options']:
                endpoint['options']['startDateSet'] = 1
                endpoint['options']['startDate'] += now
            if 'endDate' in endpoint['options'] and not 'endDateSet' in endpoint['options']:
                endpoint['options']['endDateSet'] = 1
                endpoint['options']['endDate'] += now


            urlString += "?" + urllib.urlencode(endpoint['options'])
        return urlString


class GetAppRoutesScript(tornado.web.RequestHandler):
    def get(self):
        self.set_header("Content-Type", 'text/javascript; charset="utf-8"')
        self.write("var APP_ENDPOINTS = " + tornado.escape.json_encode(appRoutes))
        self.write("var APP_ROUTES = " + tornado.escape.json_encode(appRoutes))
        return

#Define file routes
tornadoRoutes = [
    ## SPECIAL ROUTES
    (r'/app/app-routes.js', GetAppRoutesScript),

    ## FILE ROUTES
    (r"/css/(.*)", tornado.web.StaticFileHandler, {"path": os.path.join(os. path.dirname(__file__), 'css')}),
    (r'/js/(.*)', tornado.web.StaticFileHandler, {"path": os.path.join(os. path.dirname(__file__), 'js')}),
    (r'/img/(.*)', tornado.web.StaticFileHandler, {"path": os.path.join(os. path.dirname(__file__), 'img')}),
    (r'/app/(.*)', tornado.web.StaticFileHandler, {"path": os.path.join(os. path.dirname(__file__), 'app')}),
    (r'/templates/(.*)', tornado.web.StaticFileHandler, {"path": os.path.join(os. path.dirname(__file__), 'templates')}),
]

class DefineAppRoutes:

    def __init__(self, routeMap=None, routeList=None):
        self.routeMap = routeMap
        self.routeList = routeList
        print "\n\n"
        self.runLoop(self.routeMap)
        return

    def runLoop(self, mapLevel, parentRoute=None, parentPath=None):
        for route, val in mapLevel.iteritems():
            routeString = val['route'][0] if val['route'][0] == r'/' else val['route'][0] + r'/?'
            if parentPath:
                routeString = parentPath + routeString
                val['route'][0] = parentPath + val['route'][0]
            topRoute = parentRoute['children'][route] if parentRoute else self.routeMap[route]
            r = (routeString, PageHandler, {'pageOpts' : topRoute})
            self.routeList.append(r)
            if 'children' in val:
                self.runLoop(val['children'], topRoute, val['route'][0])

DefineAppRoutes(appRoutes, tornadoRoutes)

# Add the default handler after everything
tornadoRoutes.append((r'^/(.*)$', DefaultHandler))

#define the routes
application = tornado.web.Application(tornadoRoutes, debug=True)



#Start the app
if __name__ == '__main__':
    #application.listen(8888)
    application.listen(blogSettings.LISTEN_PORT)
    tornado.ioloop.IOLoop.instance().start()
