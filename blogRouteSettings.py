defaultPostsTemplate = ['pages/postListing.html']

seoDescriptions = {
	'homepage' : 'Space Ninety 8 is a unique retail concept from Urban Outfitters. Following in the footsteps of Hollywood\'s Space 15 Twenty, the Williamsburg, Brooklyn location combines retail, dining, and a place to gather.',
	'blog' : 'Read the Space Ninety 8 blog, covering events, stories, and features from the Williamsburg, Brooklyn retail concept by Urban Outfitters.',
	'gallery' : 'Gallery 98 will host several rotating pop-up shops throughout the year.',
	'marketspace' : 'Discover the Space Ninety 8 Market Space, a unique showcase of independent and local designers, one-of-a-kind finds, and vintage treasures.',
	'gorbals' : 'Ilan Hall brings his LA restaurant concept The Gorbals to Williamsburg, Brooklyn in Space Ninety 8, complete with a bar and a rooftop deck.'
}

blogEndpoints = {
	'master' : "/posts",
	'blog' : "/type/post/posts",
	'taxonomy' : "/type/post/posts",
	'marketSpace' : "/type/marketspace_type/posts",
	'gallery' : "/type/gallery_type/posts",
	'events' : "/type/event_type/posts",
	'page' : "/posts",
	'postById' : "/posts",
	'postBySlug' : "/posts/slug"
}

appRoutes = {
	'home' : {
		'title' : 'Home',
		'description' : seoDescriptions['homepage'],
		'template' : ['pages/home.html'],
		'controller': 'MainCtrl',
		'type' : "index",
		'route' : [r'/'],
		'endpoints' : {
			'splash' : {
				'method' : "getPost",
				'options' : {'postId' : 'splash'},
				'pageInfo' : { 'title' : "Space 98",  'path' : "home" }
			},
			'gorbals' : {
				'method' : "getPost",
				'options' : {'postId' : 'gorbals'},
				'pageInfo' : { 'title' : "Gorbals",  'path' : "gorbals" }
			},
			'blog' : {
				'method' : "getList",
				'endpoint': "blog",
				'options' : {'offset': 3},
				'pageInfo' : { 'title' : "Blog", 'path' : "blog" },
				'displayOptions' : { 'listingClass' : "left-align bold" }
			},
			'marketSpace' : {
				'method' : "getList",
				'endpoint': "marketSpace",
				'options' : {'offset': 3, 'date': 0},
				'pageInfo' : { 'title' : "Market Space", 'path' : "market-space" },
				'feedOptions' : { 'sortBy': "customFields.startDate" }
			},
			'gallery' : {
				'method' : "getList",
				'endpoint': "gallery",
				'options' : {'offset': 1, 'filterBy': "featuredPost"},
				'pageInfo' : { 'title' : "Gallery 98", 'path' : "gallery98" },
				'feedOptions' : { 'sortBy': "customFields.endDate" }
			}
		}
	},
	'posts' : {
		'title' : 'Posts',
		'description' : seoDescriptions['homepage'],
		'template' : ['pages/postListing.html'],
		'controller': 'MainCtrl',
		'type' : "listing",
		'route' : [r'/posts'],
		'endpoints' : {
			'posts' : {
				'method' : "getList",
				'endpoint': "master",
				'options' : {'offset' : 20 },
				'pageInfo' : { 'title' : "Posts", 'path' : "posts" },
				'displayOptions' : { 'listingClass' : "left-align bold" }
			}
		},
		'children' : {
			'singlePost' : {
				'title' : '',
				'description' : '',
				'template' : ['pages/singlePost.html'],
				'controller': 'MainCtrl',
				'type' : "post",
				'route' : [r'/(?P<postId>[^\/]+)?', '/:postId'],
				'endpoints' : {
					'post' : {
						'method' : "getPost",
						'pageInfo' : { 'path' : "" }
					},
					'related' : {
						'method' : "getList",
						'endpoint' : 'master',
						'pageInfo' : { 'title' : "Blog",  'path' : "blog" }
					}
				}
			}
		}
	},
	'blog' : {
		'title' : 'Blog',
		'description' : seoDescriptions['blog'],
		'template' : ['pages/postListing.html'],
		'controller': 'MainCtrl',
		'type' : "listing",
		'route' : [r'/blog'],
		'endpoints' : {
			'posts' : {
				'method' : "getList",
				'endpoint' : 'blog',
				'pageInfo' : { 'title' : "Blog",  'path' : "blog" }
			}
		}
	},
	'marketSpace' : {
		'title' : 'Market Space',
		'description' : seoDescriptions['marketspace'],
		'template' : ['pages/marketSpace.html'],
		'controller': 'MainCtrl',
		'type' : "index",
		'route' : [r'/market-space'],
		'endpoints' : {
			'featured' : {
				'method' : "getList",
				'endpoint' : 'marketSpace',
				'options' : { 'offset' : 1, 'filterBy' : 'featuredPost' },
				'pageInfo' : { 'title' : "Market Space", 'path' : "market-space" },
				'feedOptions' : { 'sortBy' : "customFields.startDate" }
			},
			'current' : {
				'method' : "getList",
				'endpoint' : 'marketSpace',
				'options' : { 'offset' : 9, 'date' : 0 },
				'pageInfo' : { 'title' : "Market Space", 'path' : "market-space" },
				'feedOptions' : { 'sortBy' : "customFields.startDate" }
			},
			'upcoming' : {
				'method' : "getList",
				'endpoint' : 'marketSpace',
				'options' : { 'offset' : 9, 'endDate' : 31536000, 'startDate' : 3600 },
				'pageInfo' : { 'title' : "Market Space", 'path' : "market-space" },
				'feedOptions' : { 'sortBy' : "customFields.startDate" }
			}
		},
		'children' : {
			'vendors' : {
				'title' : 'Market Space - Vendors',
				'description' : seoDescriptions['marketspace'],
				'template' : ['pages/postListing.html'],
				'controller': 'MainCtrl',
				'type' : "listing",
				'route' : [r'/vendors'],
				'endpoints' : {
					'posts' : {
						'method' : "getList",
						'endpoint' : 'marketSpace',
						'pageInfo' : { 'title' : "Market Space", 'path' : "market-space" },
						'feedOptions' : { 'sortBy' : "customFields.startDate" }
					}
				},
				'children' : {
					'vendors' : {
						'title' : 'Market Space - Vendors',
						'description' : seoDescriptions['marketspace'],
						'template' : ['pages/postListing.html'],
						'controller': 'MainCtrl',
						'type' : "listing",
						'route' : [r'/vendors'],
						'endpoints' : {
							'posts' : {
								'method' : "getList",
								'endpoint' : 'marketSpace',
								'pageInfo' : { 'title' : "Market Space", 'path' : "market-space" },
							}
						}
					}
				}
			}
		}
	},
	'gallery98' : {
		'title' : 'Gallery 98',
		'description' : seoDescriptions['gallery'],
		'template' : ['pages/gallery98.html'],
		'controller': 'MainCtrl',
		'type' : "index",
		'route' : [r'/gallery98'],
		'endpoints' : {
			'featured' : {
				'method' : "getList",
				'endpoint' : 'gallery',
				'options' : { 'offset' : 1, 'filterBy' : 'featuredPost' },
				'pageInfo' : { 'title' : "Gallery 98", 'path' : "gallery98" },
				'feedOptions' : { 'sortBy' : "customFields.startDate" }
			},
			'current' : {
				'method' : "getList",
				'endpoint' : 'gallery',
				'options' : { 'offset' : 9, 'date' : 0 },
				'pageInfo' : { 'title' : "Gallery 98", 'path' : "gallery98" },
				'feedOptions' : { 'sortBy' : "customFields.startDate" }
			},
			'upcoming' : {
				'method' : "getList",
				'endpoint' : 'gallery',
				'options' : { 'offset' : 9, 'endDate' : 31536000, 'startDate' : 3600 },
				'pageInfo' : { 'title' : "Gallery 98", 'path' : "gallery98" },
				'feedOptions' : { 'sortBy' : "customFields.startDate" }
			}
		},
		'children' : {
			'showings' : {
				'title' : 'gallery 98 - Showings',
				'description' : seoDescriptions['gallery'],
				'template' : ['pages/postListing.html'],
				'controller': 'MainCtrl',
				'type' : "listing",
				'route' : [r'/showings'],
				'endpoints' : {
					'posts' : {
						'method' : "getList",
						'endpoint' : 'gallery',
						'pageInfo' : { 'title' : "Gallery 98", 'path' : "gallery98" },
						'feedOptions' : { 'sortBy' : "customFields.startDate" }
					}
				}
			}
		}
	},
	# 'events' : {
	# 	'title' : None,
	# 	'description' : seoDescriptions['homepage'],
	# 	'template' : ['pages/postListing.html'],
	#	'route' : [r'/events/?', '/events']
	# 	'endpoints' : {
	# 		'posts' : 'type/events_type/posts'
	# 	}
	# },
	# 'gorbals' : {
	# 	'title' : 'The Gorbals',
	# 	'description' : seoDescriptions['gorbals'],
	# 	'template' : ['pages/singlePost.html', 'pages/gorbals/index.html'],
	# 	'controller': 'MainCtrl',
	# 	'type' : "page",
	# 	'route' : [r'/gorbals/?', '/gorbals'],
	# 	'innerController' : 'gorbals.index',
	# 	'endpoints' : {
	# 		'post' : {
	# 			'method' : "getPost",
	# 			'options' : { 'postId' : 'gorbals' },
	# 			'pageInfo' : { 'path' : "" }
	# 		}
	# 	}
	# },
	'gorbals' : {
		'title' : 'The Gorbals',
		'description' : seoDescriptions['gorbals'],
		'template' : ['pages/singlePost.html', 'pages/gorbals/index.html'],
		'controller': 'MainCtrl',
		'type' : "page",
		'route' : [r'/gorbals'],
		'innerController' : 'gorbals.photos',
		'endpoints' : {
			'post' : {
				'method' : "getPost",
				'options' : { 'postId' : 'gorbals-photos' },
				'pageInfo' : { 'path' : "" }
			}
		},
		'children' : {
			'photos' : {
				'title' : 'The Gorbals - Photos',
				'description' : seoDescriptions['gorbals'],
				'template' : ['pages/singlePost.html', 'pages/gorbals/photos.html'],
				'controller': 'MainCtrl',
				'type' : "page",
				'route' : [r'/photos'],
				'innerController' : 'gorbals.photos',
				'endpoints' : {
					'post' : {
						'method' : "getPost",
						'options' : { 'postId' : 'gorbals-photos' },
						'pageInfo' : { 'path' : "" }
					}
				}
			},
			'menu' : {
				'title' : 'The Gorbals - Menu',
				'description' : seoDescriptions['gorbals'],
				'template' : ['pages/singlePost.html', 'pages/gorbals/menu.html'],
				'controller': 'MainCtrl',
				'type' : "page",
				'route' : [r'/menu'],
				'innerController' : 'gorbals.menu',
				'endpoints' : {
					'post' : {
						'method' : "getPost",
						'options' : { 'postId' : 'gorbals-menu' },
						'pageInfo' : { 'path' : "" }
					}
				}
			}
		}
	},
	'test' : {
		'title' : 'The Gorbals',
		'description' : seoDescriptions['gorbals'],
		'template' : ['pages/test.html'],
		'controller': 'MainCtrl',
		'type' : "page",
		'route' : [r'/test'],
		'innerController' : 'gorbals.photos',
		'endpoints' : {}
	}
}
